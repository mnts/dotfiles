export ZSH=/home/mantas/.oh-my-zsh
export VISUAL=nano
export EDITOR="$VISUAL"
export PATH="/usr/local/sbin:$PATH"
export DENO_INSTALL="/home/mantas/.deno"
export PATH="$DENO_INSTALL/bin:$PATH"
export GRIM_DEFAULT_DIR="/home/mantas/Pictures/"
export TERM=xterm
