# Setting and editing of env variables.

# Vars
export EDITOR="/bin/nano"
export SUDO_EDITOR='/bin/nano'
export VISUDO='/bin/nano'

# PATH
#export PATH=$PATH:~/.dotfiles/bin
#export PATH=$PATH:~/.npm-global/bin # npm globally installed
export PATH=$PATH:$HOME/.local/bin
export GOPATH=$HOME/go
#. ~/.nix-profile/etc/profile.d/nix.sh # Nix working

typeset -U PATH # Remove duplicates in $PATH
export GRIM_DEFAULT_DIR="/home/mantas/Pictures/Screenshots"
export SWAYSHOT_SCREENSHOTS="/home/mantas/Pictures/Screenshots"
export XDG_PICTURES_DIR="/home/mantas/Pictures"
export TERM=xterm
export MOZ_ENABLE_WAYLAND=1
export XDG_CURRENT_DESKTOP=sway