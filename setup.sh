#!/bin/bash

# ZSH
ln -s /home/mantas/.dotfiles/zsh/zshrc.zsh /home/mantas/.zshrc
# ROFI
ln -s /home/mantas/.dotfiles/rofi /home/mantas/.config/rofi 
# i3
ln -s /home/mantas/.dotfiles/i3 /home/mantas/.config/i3 
# PICOM (compton)
ln -s /home/mantas/.dotfiles/picom /home/mantas/.config/picom 
# ALACRITTY
ln -s /home/mantas/.dotfiles/alacritty /home/mantas/.config/alacritty 
# POLYBAR
ln -s /home/mantas/.dotfiles/polybar /home/mantas/.config/polybar 
# SWAY
ln -s /home/mantas/.dotfiles/sway /home/mantas/.config/sway 
# SWAYBAR
ln -s /home/mantas/.dotfiles/waybar /home/mantas/.config/waybar 
# MAKO
ln -s /home/mantas/.dotfiles/mako /home/mantas/.config/mako